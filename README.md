# Database Management

This repository contains a project documentation about the analysis of the political activity in the Swiss national council using Python and SQL. 

Used libraries are among others:
* Selenium (web scraping)
* BeautifulSoup (web scraping)
